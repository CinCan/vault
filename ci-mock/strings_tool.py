#!/usr/bin/env python3
import json, sys, subprocess, re

def emit_output(**kwargs):
    json.dump(kwargs, sys.stdout, indent=2, sort_keys=True)
    sys.stdout.write("\n")


def main():
    if len(sys.argv) != 2:
        sys.exit("usage: strings_tool.py input_filename")
    filename = sys.argv[1]
    strings_output = subprocess.check_output(["strings", "-a", "--", filename]).decode("utf-8")
    urls = re.findall(r'https?://.*', strings_output)
    def url_tax_entry(u):
        return dict(namespace="string-in-binary",
                        predicate="url",
                        value=u,
                        level="info")
    
    emit_output(success=True, artifacts=urls,
                summary=dict(taxonomies=list(map(url_tax_entry, urls)),
                full=strings_output))

if __name__ == '__main__':
    main()
