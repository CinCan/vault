#!/usr/bin/env python3

import subprocess, sys, tempfile, os, time, hashlib
import atexit, shutil
import json

def just_show_digest(path):
    "This is called in place of an external tool, when no tool is provided. For testing."
    hex_digest = hashlib.new("sha256", open(path, 'rb').read()).hexdigest()
    print("**", dict(filename=path, hash="sha256:"+hex_digest))

def run_tool_on_file(data_path, tool_path):
    resp_data = subprocess.check_output([tool_path, data_path])
    if resp_data:
        print("tool returned:", json.loads(resp_data))
    else:
        print("tool failed to return data")
                      
def run_git(*git_args):
    result = subprocess.run(["git", *git_args])
    if result.returncode != 0:
        sys.exit("git clone failed")
    return result

def get_git_output(*git_args):
    result = subprocess.Popen(["git", *git_args], stdout=subprocess.PIPE)
    return result.stdout.read().decode("utf-8")

def get_current_git_revision():
    return get_git_output("rev-parse", "HEAD").strip()

def get_shell_output(shellcmd):
    result = subprocess.Popen(shellcmd, stdout=subprocess.PIPE, shell=True)
    return result.stdout.read()

def get_changed_files(oldrev, newrev):
    cmd = ("git log --name-only --pretty=oneline --full-index "
               + str(oldrev) + ".." + str(newrev)
               + " | grep -vE '^[0-9a-f]{40} ' | sort | uniq")
    print("running: ", cmd)
    return get_shell_output(cmd).decode("utf-8").splitlines()
    
def main():
    watched_branch = "master"
    if len(sys.argv) == 2:
        repo_url, tool_path = sys.argv[1], None
    elif len(sys.argv) == 3:
        repo_url, tool_path = sys.argv[1:]
        tool_path = os.path.abspath(tool_path) # path needs to survive chdir
    else:
        sys.exit("usage: gitreact.py repo_url [tool_path]")
    work_dir = tempfile.mkdtemp(prefix="gitreact.")
    atexit.register(lambda: shutil.rmtree(work_dir))
    repo_dir =os.path.join(work_dir, "gitrepo")
    run_git("clone", repo_url, repo_dir)
    os.chdir(repo_dir)

    last_seen_revision = prev_seen_revision = None
    while True:
        # get latest changes from source repo 
        run_git("fetch", "-q", "origin")
        # check them out in a temp branch
        run_git("checkout", "-q", "-b", "tempbranch", "origin/" + watched_branch)
        prev_seen_revision = last_seen_revision
        last_seen_revision = get_current_git_revision()
        if ((last_seen_revision and prev_seen_revision) 
                and last_seen_revision != prev_seen_revision):
            changed = get_changed_files(prev_seen_revision, last_seen_revision)
            print("changes happened in", changed)
            for filename in changed:
                if tool_path:
                    run_tool_on_file(filename, tool_path)
                else:
                    just_show_digest(filename)
        else:
            print("no changes")
        run_git("checkout", "-q", watched_branch)
        run_git("branch", "-qD", "tempbranch")
        print("sleeping")
        time.sleep(1)
                    
if __name__ == '__main__':
    main()
