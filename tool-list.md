## Static analysis

* [CIML](https://github.com/mtreinish/ciml)
* [Rekall Forensics](http://www.rekall-forensic.com/)
* [Manalyze](https://github.com/JusticeRage/Manalyze)
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/manalyze/Dockerfile)
* [flawfinder](https://dwheeler.com/flawfinder/)
    * [*DockerFile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/flawfinder/Dockerfile)
* [Binwalk](https://github.com/ReFirmLabs/binwalk)
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/binwalk/Dockerfile)
* [binaryanalysis BAT](http://www.binaryanalysis.org/)
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/binary-analysis-tool-bat/Dockerfile)
* [BinSkim](https://github.com/Microsoft/binskim)                               
    * [*Dockerfile (Windows)*](https://gitlab.com/CinCan/dockerfiles/tree/master/binskim/Dockerfile)
* [Jakstab](https://github.com/jkinder/jakstab) 
    * [*Dockerfile (Windows)*](https://gitlab.com/CinCan/dockerfiles/tree/master/jakstab/Dockerfile)
* [Twiggy](https://github.com/rustwasm/twiggy)
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/twiggy/Dockerfile)
* [truffleHog](https://github.com/dxa4481/truffleHog)
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/trufflehog/Dockerfile)
* [AIL framework](https://github.com/CIRCL/AIL-framework)
* [keyfinder](https://github.com/CERTCC/keyfinder)
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/keyfinder/Dockerfile)
* [Android Emulation Toolkit (AET)](https://github.com/nixu-corp/aet)
* [oledump](https://blog.didierstevens.com/programs/oledump-py/)
    * [*Dockerfile (Windows)*](https://gitlab.com/CinCan/dockerfiles/tree/master/oledump/Dockerfile)
    * *Dockerfile (Linux) - Ville K.*
* [IREC](https://binalyze.com/products/irec-free/)
    * *Supports only Windows operating systems*
    * *Does not have CLI, controlling can be made only via GUI*
* [Assemblyline](https://cyber.gc.ca/en/assemblyline)
* [Viper](https://github.com/viper-framework/viper)
* [pestudio](https://www.winitor.com/)
   * [*Dockerfile (Windows)*](https://gitlab.com/CinCan/dockerfiles/tree/master/pestudio/Dockerfile)
* [MASTIFF](https://git.korelogic.com/mastiff.git/)
* [angr](https://github.com/angr/angr)
* [Strings](https://sourceware.org/binutils/docs/binutils/strings.html)
* [Objdump](https://www.systutorials.com/docs/linux/man/1-objdump/)
* [IDA](https://www.hex-rays.com/products/ida/support/download.shtml)
* [Radare](https://rada.re/r/)
* [JAD](http://www.javadecompilers.com/jad)

## Packer analysis

* [RDG](http://www.rdgsoft.net/)
* [PEiD](https://www.softpedia.com/get/Programming/Packers-Crypters-Protectors/PEiD-updated.shtml)
* [PACKERID](https://code.google.com/archive/p/malwaremustdie/downloads)
* [LANGUAGE 2000](https://farrokhi.net/language/)
    * *Supports only Windows operating systems*
    * *GUI ONLY*
* [ExeScan](https://securityxploded.com/exe-scan.php)
* [Q-UNPACK](https://authorne.info/dkpWMXUXKCVFSgM4OgwdAj4mQlBFC3MDM1N4EFUTHyYzQlsTP3MDMxAjOlQGU3gQBgJAeGNLDxcu)

## Online tools and malware analysis

* [AlienVault](https://www.alienvault.com/)
* [Domaintools](https://www.domaintools.com/)
* [censys](https://censys.io/)
* [Shodan](https://www.shodan.io/)
* [ThreatConnect](https://threatconnect.com/)
* [PassiveTotal](https://www.riskiq.com/products/passivetotal/)
* [VirusTotal](https://www.virustotal.com/#/home/upload)
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/virustotal/Dockerfile)
* [VirSCAN](http://www.virscan.org/)
* [malwr (temporarily unavailable)](https://malwr.com/)
* [MalwareViz](https://www.malwareviz.com/)
* [ViCheck](https://www.vicheck.ca/)
* [MetaDefender](https://metadefender.opswat.com/#!/)

## Document analysis

* [OfficeMalScanner](https://www.aldeid.com/wiki/OfficeMalScanner/OfficeMalScanner)
    * [*Dockerfile (Windows)*]https://gitlab.com/CinCan/dockerfiles/tree/master/officemalscanner/Dockerfile)
* [OffVis](http://go.microsoft.com/fwlink/?LinkId=158791)
    * *Supports only Windows operating systems*
    * *Does not have CLI, controlling can be made only via GUI*
* [Cryptam](http://www.malwaretracker.com/doc.php)
* [PDF Examiner](http://www.malwaretracker.com/pdf.php)
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/pdfexaminer/Dockerfile)
* [PDF Tools](https://blog.didierstevens.com/programs/pdf-tools/)
    * [*Dockerfile (Windows)*](https://gitlab.com/CinCan/dockerfiles/tree/master/pdf-parser/Dockerfile)
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/pdf-tools/Dockerfile)
* [PDF X-RAY](https://github.com/9b/pdfxray_public)
* [PDF X-RAY lite](https://github.com/9b/pdfxray_lite)
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/pdfxray_lite/Dockerfile)
* [peepdf](https://github.com/jesparza/peepdf)
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/peepdf/Dockerfile)
* [origami-pdf](https://code.google.com/archive/p/origami-pdf/)
* [PDF Stream Dumper](http://sandsprite.com/blogs/index.php?uid=7&pid=57)
    * [*Dockerfile (Windows)*](https://gitlab.com/CinCan/dockerfiles/tree/master/pdf-stream-dumper/Dockerfile)
* [Vba2Graph](https://github.com/MalwareCantFly/Vba2Graph)
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/vba2graph/Dockerfile)
* [PDF-Parser](https://pdfparser.org/)
    * *Dockerfile (Linux) - Ville K.*
* [Pdfid](https://blog.didierstevens.com/2009/03/31/pdfid/)
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/pdfid/Dockerfile)
* [Pdf2john](https://github.com/magnumripper/JohnTheRipper)
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/pdf2john/Dockerfile)

## Browser malware analysis

* [jsunpack-n](https://github.com/urule99/jsunpack-n)
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/jsunpack-n/Dockerfile)

## System and file analysis

* [Sysinternals](https://docs.microsoft.com/en-us/sysinternals/)
    * [*Dockerfile (Windows)*](https://gitlab.com/CinCan/dockerfiles/tree/master/sysinternals/Dockerfile)
* [regshot](https://sourceforge.net/projects/regshot/)
    * *Supports only Windows operating systems*
    * *Currently does not have CLI (v. 1.9.1), controlling can be made only via GUI*
    * *Version 1.9.2 should include also CLI*
    * [*Dockerfile (Windows)*](https://gitlab.com/CinCan/dockerfiles/tree/master/regshot/Dockerfile) *<- can be used if/when the new version is released*
* [Capture BAT](http://www.honeynet.org/project/CaptureBAT)
    * [*Dockerfile (Windows)*](https://gitlab.com/CinCan/dockerfiles/tree/master/capture-bat/Dockerfile)
* [SysAnalyzer](https://github.com/dzzie/SysAnalyzer)
    * [*Dockerfile (Windows)*](https://gitlab.com/CinCan/dockerfiles/tree/master/sysanalyzer/Dockerfile)
    * *SysAnalyzer does not fully support CLI (there are only few CLI commands that can be used)*
* [Process Hacker](https://processhacker.sourceforge.io/)
    * [*Dockerfile (Windows)*](https://gitlab.com/CinCan/dockerfiles/tree/master/processhacker/Dockerfile)
    * *Process Hacker does not fully support CLI (there are only few CLI commands that can be used)*
* [gmer](http://www.gmer.net/)
    * *Supports only Windows operating systems*
    * *Does not have CLI, controlling can be made only via GUI*
* [procdot](http://procdot.com/)
* [RadioGraphy](http://www.security-projects.com/?RadioGraPhy)
* [Noriben](https://github.com/Rurik/Noriben)
* [API Monitor](http://www.rohitab.com/apimonitor)
    * *Supports only Windows operating systems*
    * *Does not have CLI, controlling can be made only via GUI*
* [Sysmon Tools](https://github.com/nshalabi/SysmonTools)
    * *Supports only Windows operating systems*
    * *Does not have CLI, controlling can be made only via GUI*

## Shellcode analysis

* [ShellDetect](https://securityxploded.com/shell-detect.php)
* [libemu](https://github.com/buffer/libemu)
* [shellcode2exe](http://sandsprite.com/shellcode_2_exe.php)  
* [shellcode2exe.py](https://github.com/MarioVilas/shellcode_tools) 
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/shellcode2exe/Dockerfile)

* [ConvertShellcode](https://github.com/arioux/ConvertShellcode)
    * [*Dockerfile (Windows)*](https://gitlab.com/CinCan/dockerfiles/tree/master/convertshellcode/Dockerfile)
* [Shellcode Analysis](http://www.malwaretracker.com/shellcode.php)
* [jmp2it](https://github.com/adamkramer/jmp2it/)
* [scdbg shellcode analysis](https://isc.sans.edu/diary/rss/24058)

## Feeds

* [PhisTank](https://www.phishtank.com/)
* [Abuse.ch](https://abuse.ch/)
* [VXVault](https://github.com/InfectedPacket/VxVault)

## Memory forensics

* [Volatility](https://github.com/volatilityfoundation/volatility)
* [Volatilitux](https://code.google.com/archive/p/volatilitux/)
* [LiME](https://github.com/504ensicsLabs/LiME)
* [Memoryze](https://www.fireeye.com/services/freeware/memoryze.html)
* [Redline](https://www.fireeye.com/services/freeware/redline.html)
* [VolUtility](https://github.com/kevthehermit/VolUtility)

## Malware analysis tool lists

* [rshipp github](https://github.com/rshipp/awesome-malware-analysis)
* [hslatman github](https://github.com/hslatman/awesome-threat-intelligence)
* [malware-analyzer.com](http://www.malware-analyzer.com/analysis-tools)


## Pcap
 * [Suricata](https://suricata-ids.org/)
     * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/suricata/Dockerfile)
 * [Zeek / Bro](https://zeek.org/)

## Other software

* [Botnet source codes](https://github.com/maestron/botnets)
* [osquery](https://osquery.io/)
* [GTFOBins](https://gtfobins.github.io/#)
* [cuckoo](https://hub.docker.com/r/blacktop/cuckoo/)
* [Cortex Analyzer](https://github.com/TheHive-Project/Cortex-Analyzers)
* [DEPENDENCY-TRACK](https://dependencytrack.org/)
* [yara](https://github.com/virustotal/yara)
* [Inav](http://lnav.org/)
* [ANY RUN](https://any.run/)
* [Joe Sandbox](https://www.joesecurity.org/)
* [Sandboxed Execution Environment](https://github.com/F-Secure/see)
* [OllyDbg](http://www.ollydbg.de/)
* [WinDbg](https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/)
* [WireShark](https://www.wireshark.org/)
* [Hybrid Analysis](https://www.hybrid-analysis.com/)
* [Hashcat](https://www.hashcat.net/)
    * [*dizcza/docker-hashcat By dizcza (Linux)*](https://hub.docker.com/r/dizcza/docker-hashcat)
* [iocextract](https://inquest.readthedocs.io/projects/iocextract/en/latest/)
    * [*Dockerfile (Linux)*](https://gitlab.com/CinCan/dockerfiles/tree/master/iocextract/Dockerfile)
