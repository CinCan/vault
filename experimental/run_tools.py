#!/usr/bin/env python3

import subprocess, sys
import json


def run_tool(tool, filename, tool_args):
    print("Running '{}' on '{}' with arguments {}".format(tool, filename, tool_args))
    result = subprocess.Popen([tool, filename] + tool_args, stdout=subprocess.PIPE)
    return (result.stdout.read())

def run_all_tools(tools):
    for tool, files in tools.items():
        for filename, args in files["files"].items():
            run_tool(tool, filename, args)

def get_tools(config):
    with open(config) as c:
        return json.load(c)

def main():
    config = "testconf"
    tools = get_tools(config)
    print(run_all_tools(tools))

if __name__ == '__main__':
   main()