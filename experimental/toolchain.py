#!/usr/bin/env python3

import subprocess

def pipe(toolchain):
    """Run the commandline tools listed in the toolchain, piping the results
    to the next tool in chain.
    input: list of tools to run with their arguments, e.g. ['ls -a', 'grep "something"'']
    """
    try:
        tool_before = toolchain[0].split(" ")

        process_before = subprocess.Popen(tool_before, 
            stdin=None,
            stdout = subprocess.PIPE,
            stderr = subprocess.PIPE)
        previous = process_before

        for tool in toolchain[1:]:
            tool = tool.split(" ")
            process_after = subprocess.Popen(tool,
                stdin=previous.stdout,
                stdout = subprocess.PIPE,
                stderr = subprocess.PIPE)
            previous = process_after

        stdout, stderr = process_after.communicate()
        process_after.wait()
        returncode = process_after.returncode

    except Exception, e:
        stderr = str(e)
        returncode = -1

    if returncode == 0:
        return (True, stdout)
    else:
        return (False, stderr)