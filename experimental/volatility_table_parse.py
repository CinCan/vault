from pprint import pprint
columns = []
rows = []
add_to_table = False
tables = []
with open("dlllist_sample") as f:
    f = f.read().splitlines()
    for i, j in enumerate(f):
        begin = j.replace(" ", "").count("-") == len(j.replace(" ", ""))
        end = j.replace(" ", "").count("*") == len(j.replace(" ", ""))
        if end:
            tables += [columns, [rows]]
            add_to_table = False
        elif begin:
            columns = f[i-1].split()
            add_to_table = True
        else:
            if add_to_table:
                rows.append(j.split())
