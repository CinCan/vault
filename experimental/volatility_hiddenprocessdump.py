#!/usr/bin/env python3

import subprocess


def hidden_processes(sample, volatility_path, *args):
    print("Finding hidden processes in '{}'".format(sample))
    print("python", volatility_path, "psxview", sample, *args)
    result = subprocess.check_output(["python", volatility_path, "psxview", "-f", sample, *args], universal_newlines=True)
    processes = result.split("\n")
    columns = processes[0].split()
    hidden_processes = []
    for i in processes[2:]:
        process = i.split()
        if ('False' in process[3:9]):
            hidden_processes.append(process)
    return columns, hidden_processes

def get_profile(sample, volatility_path):
    print("Finding the memory dump profile for {}".format(sample))
    result = subprocess.check_output(["python", volatility_path, "kdbgscan", "-f", sample], universal_newlines=True)
    result = result.split("\n")
    suggested_profiles = []
    for i in result:
        if ("Profile suggestion" in i):
            suggested_profiles.append(i.split(": ")[1])
    return suggested_profiles

def dump_process(sample, output_path, pID, offset)
    print("Dumping the executable of pID {} to {}".format(pID, output_path))
    subprocess.Popen(["python", volatility_path, "procdump", "-D",
        output_path, "-f", sample, "-p", pID,"--offset={}".format(offset)])