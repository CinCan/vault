> Docker images are pulled from CinCan Docker Hub repo (https://hub.docker.com/u/cincan/)

1 - Verify that Docker is installed  
    `$ docker -v`  

2 - Is $USER in Docker group?  
    `$ grep 'docker' /etc/group`  
    Add user to group if necessary:    
    `$ sudo usermod -aG docker $USER` 
    
3 - Run the tool for the sample  

### Binwalk

  tool help:  
    `$ docker run cincan/binwalk --help`  
  example:  
    `$ docker run -v /samples:/samples cincan/binwalk -eC /samples/output /samples/input/dropper.exe`
    
### Suricata

  example:  
    `$ docker run --rm -v /samples/input:/pcaps -v /samples/output/suricata_logs:/logs cincan/suricata -r /pcaps -l /logs`
    
---

> You can also build a local image from a Dockerfile:

### Pdfid
    
  Go to your directory with the Dockerfile and build the image:  
    `$ docker build -t cincan/pdfid .`  
    
  Analyze one sample, include triage -plugin:  
    `$ docker run -v /samples:/samples cincan/pdfid -p plugin_triage /samples/input/sample.pdf`
    
    